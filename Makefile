LIB= request

SRCS+= srq-request.c
SRCS+= srq-tuple.c
SRCS+= srq-tuples.c
SRCS+= srq-file.c
SRCS+= srq-files.c

MK_PROFILE=no
MK_MAN=no
NO_OBJ=1

WARNS=2

.include <bsd.lib.mk>
